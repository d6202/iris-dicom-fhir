#FROM intersystemsdc/iris-community
FROM store/intersystems/irishealth-community:2021.2.0.649.0

USER root

ENV DEBIAN_FRONTEND noninteractive


# install stuff
RUN apt-get -y update \
    && apt-get -y install apt-utils \
    && apt-get install -y build-essential unzip pkg-config \
        zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev \
        libssl-dev libreadline-dev libffi-dev wget \
    && apt-get install -y ffmpeg libsm6 libxext6 \
    && apt-get install -y python3-pip   

# use pip3 (the python zpm) to install dicom2fhir, etc
RUN pip3 install --upgrade pip setuptools~=50.3.2 wheel
RUN pip3 install --target /usr/irissys/mgr/python requests pydicom==2.0.0 numpy==1.19.3 pillow==8.1.1 scipy==1.4.1 h5py==2.10.0 matplotlib==3.3.2 opencv-python
RUN pip3 install --target /usr/irissys/mgr/python fhir.resources==5.1.1 orjson

USER root   
WORKDIR /opt/irisbuild
RUN chown ${ISC_PACKAGE_MGRUSER}:${ISC_PACKAGE_IRISGROUP} /opt/irisbuild
USER ${ISC_PACKAGE_MGRUSER}

WORKDIR /opt/irisbuild
COPY input input
COPY output output
COPY src src
COPY module.xml module.xml
COPY iris.script iris.script
COPY wheels wheels

RUN pip3 install --target /usr/irissys/mgr/python wheels/dicom2fhir-0.0.8-py3-none-any.whl 

USER root
RUN chmod -R 777 input
RUN chmod -R 777 output

USER ${ISC_PACKAGE_MGRUSER}

# FHIR Accelerator Service
ENV APIKEY "n3bzkgvKsd6qJU4frj1iI8vYqbWO5TVa9w4GxFLG"
ENV FHIRENDPOINT "https://fhir.h15hcr06anjb.workload-nonprod-fhiraas.isccloud.io"

RUN iris start IRIS \
	&& iris session IRIS < iris.script \
    && iris stop IRIS quietly
