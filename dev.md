# useful commands
## clean up docker 
```
docker system prune -f
```

## build container with no cache
```
docker-compose build --no-cache
```
## start iris container
```
docker-compose up -d
```

## open iris terminal in docker
```
docker-compose exec iris-dicom-fhir iris session iris -U USER
```

