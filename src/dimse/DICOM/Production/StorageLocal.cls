Include EnsDICOM

/// This production demonstrates how to receive C-STORE requests and store them as
/// DICOM files in the ensemble file structure
Class dimse.DICOM.Production.StorageLocal Extends Ens.Production
{

XData ProductionDefinition
{
<Production Name="dimse.DICOM.Production.StorageLocal" TestingEnabled="true" LogGeneralTraceEvents="true">
  <Description></Description>
  <ActorPoolSize>2</ActorPoolSize>
  <Setting Target="Production" Name="ShutdownTimeout">120</Setting>
  <Setting Target="Production" Name="UpdateTimeout">10</Setting>
  <Setting Target="Production" Name="StorageLocation">/opt/irisbuild/input</Setting>
  <Item Name="dimse.DICOM.Service.SCP" Category="" ClassName="EnsLib.DICOM.Service.TCP" PoolSize="1" Enabled="true" Foreground="false" Comment="" LogTraceEvents="true" Schedule="">
    <Setting Target="Host" Name="InactivityTimeout">0</Setting>
    <Setting Target="Host" Name="DuplexTargetConfigName">dimse.DICOM.Process.StorageLocal</Setting>
    <Setting Target="Adapter" Name="LocalAET">DIMSE</Setting>
    <Setting Target="Adapter" Name="RemoteAET">MESCU</Setting>
    <Setting Target="Adapter" Name="TraceVerbosity">2</Setting>
    <Setting Target="Adapter" Name="JobPerConnection">0</Setting>
    <Setting Target="Adapter" Name="IPPort">5000</Setting>
    <Setting Target="Adapter" Name="CallInterval">1</Setting>
  </Item>
  <Item Name="dimse.DICOM.Process.StorageLocal" Category="" ClassName="dimse.DICOM.Process.StorageLocal" PoolSize="1" Enabled="true" Foreground="false" Comment="" LogTraceEvents="false" Schedule="">
    <Setting Target="Host" Name="InactivityTimeout">0</Setting>
  </Item>
  <Item Name="dimse.DICOM.Operation.File.Fhiraas" Category="" ClassName="dimse.DICOM.Operation.File.Fhiraas" PoolSize="1" Enabled="true" Foreground="false" Comment="" LogTraceEvents="false" Schedule="">
  <Setting Target="Host" Name="FileStorageDirectory">/opt/irisbuild/input</Setting>
  </Item>
</Production>
}

Parameter SETTINGS = "ShutdownTimeout,UpdateTimeout,StorageLocation";

/// This is the storage location for the DICOM streams to be stored
Property StorageLocation As %String;

/// This is a setup method which configures DICOM for the demo.
ClassMethod Setup()
{
 	Write !,"Configure DICOM Storage for DIMSE"
	
	#; We will be accepting Storage and Query requests from * (Comma is ALL)
	Do ##class(EnsLib.DICOM.Util.AssociationContext).CreateAssociation("MESCU","DIMSE",)
		
	Quit
}

/// Override this in your Production class to do setup before the Production starts
ClassMethod OnStart(pTimeStarted As %String) As %Status
{
	#; Make sure that the associations exist
	If '##class(EnsLib.DICOM.Util.AssociationContext).AETExists("MESCU","DIMSE")
	{
    #; (Comma is ALL)
		Do ##class(EnsLib.DICOM.Util.AssociationContext).CreateAssociation("MESCU","DIMSE",)
	}
	
	Quit $$$OK
}

}
