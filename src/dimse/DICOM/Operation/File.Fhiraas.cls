Class dimse.DICOM.Operation.File.Fhiraas Extends Ens.BusinessOperation
{

/// This is the directory where the local files will be stored
Parameter SETTINGS = "FileStorageDirectory";

/// This is the directory where the incoming DICOM files will be stored
Property FileStorageDirectory As %String(MAXLEN = "") [ InitialExpression = "/" ];

/// This is the default message handler.  All request types not declared in the message map are delivered here
Method OnMessage(pRequest As %Library.Persistent, Output pResponse As %Library.Persistent) As %Status
{
	#dim tSC As %Status = $$$OK
	#dim e As %Exception.AbstractException
	#dim tFile As EnsLib.DICOM.File
	#dim tFileName As %String
	try {
		
		#; We should only ever see DICOM Documents here
		$$$ASSERT(pRequest.%Extends("EnsLib.DICOM.Document"))
		$$$LOGINFO(pRequest.GetValueAt("DataSet.SOPInstanceUID"))
		Set tStudyInstanceUID = pRequest.GetValueAt("DataSet.SOPInstanceUID")
		#; Create a DICOM File from the DICOM document
		Set tSC=##class(EnsLib.DICOM.File).CreateFromDocument(pRequest,.tFile)
		If $$$ISERR(tSC) Quit
		Set tSC = ##class(%Library.File).CreateDirectory(..FileStorageDirectory_"/"_tStudyInstanceUID)
		#; Create a unique filename
		Set tFileName=..NewFilename(..FileStorageDirectory_"/"_tStudyInstanceUID)

		$$$LOGINFO(tFileName)
		#; Create a new file with a unique name and the dcm extension in the target directory
		Set tSC=tFile.Save(tFileName)

		#; convert to fhir
		Set tResponse = ..Dicom2Fhir(..FileStorageDirectory_"/"_tStudyInstanceUID)
		$$$LOGINFO("Conversion Response: "_tResponse)
		
	} catch(e) {
		Set tSC=e.AsStatus()
	}
	Quit tSC
}

/// Create a new file name within the specified directory
ClassMethod NewFilename(dir) As %String [ CodeMode = expression ]
{
##class(%File).NormalizeDirectory(dir)_(##class(%FileBinaryStream).NewFileName("dcm"))
}

ClassMethod Dicom2Fhir(pStudyDir) As %String [ Language = python ]
{
	import os
	import dicom2fhir
	import orjson
	import json
	import requests
	from fhir import resources as fr

    # process_dicom_2_fhir
	imaging_study_fhir = dicom2fhir.main.process_dicom_2_fhir(pStudyDir)
	
	api_key = os.environ['APIKEY']
	endpoint = os.environ['FHIRENDPOINT']

	url = endpoint + '/ImagingStudy'
	headers = {'x-api-key': '{key}'.format(key=api_key)}

	fhirpoststudy = requests.post(url, headers=headers, json=imaging_study_fhir.as_json())
	return fhirpoststudy

}

}
