
## DIMSE Iris Dicom Fhir Service
This is an application to ingest dicom medical images images using dimse operations and convert the studies to FHIR resources from InterSystems IRIS Embedded Python and send them to the FHIR Accelerator Service.

<p align="center">
  <img src="assets/iris-dicom-fhir.png">
</p>

## Quick Start
1. Navigate over to https://portal.live.isccloud.io, open an account, provision the InterSystems FHIR Accelerator Service.  
2. Create an API Key, record it.  
3. In the Overview page, record the FHIR Endpoint.  
4. Update Dockerfile with APIKEY and FHIRENDPOINT environment variables.

## Installation
1. Clone/git pull the repo into any local directory

```
$ git clone https://gitlab.com/d6202/iris-dicom-fhir.git
```
2. Update Dockerfile with FHIR API Key and FHIR Endpoint.  
   
3. Open a Docker terminal in this directory and run:

```
$ docker-compose build
```

4. Run the IRIS container:

```
$ docker-compose up -d 
```
# Take it for a Spin
By default, the solution has a DICOM Service Class Provider (SCP) listening on port 5000, with an AETITLE of "DIMSE".  The solution has contexts and an association prepared to allow a calling AETITLE of "MESCU".

Using any SCU software, such as the below [dcmtk](https://dcmtk.org/dcmtk.php.en) binary, you can send an image in using the following:

<p align="center">
  <img src="assets/dcmtk-send.png">
</p>

<details closed>
<summary>Terminal</summary>
<br>

```
(base) sween @ dimsecloud-pop-os ~/Desktop
└─ $ ▶ /usr/bin/storescu -v -aet "MESCU" -aec "DIMSE" localhost 5000 CT_Anno.dcm 
I: checking input files ...
I: Requesting Association
I: Association Accepted (Max Send PDV: 16372)
I: Sending file: /home/sween/Desktop/DICOM/CT/CT_Anno.dcm
I: Converting transfer syntax: Little Endian Implicit -> Little Endian Implicit
I: Sending Store Request (MsgID 1, CT)
XMIT: .................................
I: Received Store Response (Success)
I: Releasing Association

```
</details>

Then, inspect your FHIR repository by doing a GET against the /ImagingStudy resource and your message trace in IRIS.

# About
This service is an example derived and launched on [DIMSE Cloud](https://www.dimsecloud.com).

# Credits
This application uses the InterSystems IRIS Embedded Python and the [FHIR Accelerator Service](https://portal.live.isccloud.io), additionally uses a modified python library of [dicom2fhir](https://github.com/sween/dicom-fhir-converter).
